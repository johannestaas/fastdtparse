
s = '2016-12-13T01:07:22'

def test_stdlib(benchmark):
    from datetime import datetime
    fmt = '%Y-%m-%dT%H:%M:%S'
    benchmark(datetime.strptime, s, fmt) 

def test_rust(benchmark):
    from fastdtparse import parse_isoformat
    benchmark(parse_isoformat, s)

